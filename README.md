LigoMap
===========

A simple ORM-style service for Neo4J.
**Under construction. Look at the API before using.**

How to use this?
================
- Clone the repo (or request for direct access to me).
- Make sure that you have JDK 1.7, and Maven 3+. 
- A simple `mvn clean test` will run all the unit and integration tests.

How do I make changes to this lib?
==================================
- Send me a pull request. If you have direct access, then I guess, I trust you enough to make commits without consulting me!

Can you add/remove a feature for me?
====================================
Sure. Create a issue or email me directly.

License
================
Copyright 2014 Aniket Dahotre.

Licensed under the MIT License