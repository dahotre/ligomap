package me.roar.model.node;

/**
 * Marker interface
 */
public interface Animal {
  public String getName();
}
