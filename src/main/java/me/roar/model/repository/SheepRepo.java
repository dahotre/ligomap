package me.roar.model.repository;

import ligo.repository.EntityRepo;
import me.roar.model.node.Animal;
import me.roar.model.node.Sheep;
import me.roar.model.relationship.Follows;
import org.neo4j.graphdb.Direction;

import java.util.Date;
import java.util.Set;

public class SheepRepo extends EntityRepo {
  public SheepRepo() {
    super();
  }

  public Sheep create(Sheep sheep) {
    sheep.setCreatedAt(new Date());
    return save(sheep);
  }

  public void follows(Sheep sheep, Animal animal) {
    addRelatives(sheep, new Follows(null, Direction.OUTGOING), animal);
  }

  /**
   * Get all the animals that the given stupid sheep follows
   *
   * @param sheep the stupid sheep
   * @return All the leading animals
   */
  public Set<Animal> getLeaders(Sheep sheep) {
    return getRelatives(sheep, new Follows(null, Direction.OUTGOING));
  }

  public void deleteByName(final String name) {
    delete(Sheep.class, "name", name);
  }
}
