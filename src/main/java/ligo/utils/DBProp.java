package ligo.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author hsaqallah
 */
public class DBProp {

    private static final Properties PROPERTIES;
    public static final String DB_PROP_FILE_PATH = "DB_PROP_FILE_PATH";

    static {
        String dbPropFilePath = getEnvProperty(DB_PROP_FILE_PATH);
        PROPERTIES = new Properties();
        try {
            PROPERTIES.load(new FileInputStream(dbPropFilePath));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static String getDBProperty(String propertyName) {
        String propertyValue = PROPERTIES.getProperty(propertyName);
        if (propertyValue == null || propertyValue.isEmpty()) {
            throw new DBPropException(propertyName);
        }
        return propertyValue;
    }

    private static String getEnvProperty(String propKey) {
        String propValue = System.getProperty(propKey);
        if (propValue == null || propValue.equals("")) {
            throw new RuntimeException("Property " + propKey + "  undefined.");
        }
        return propValue;
    }
}
