package ligo.utils;

import ligo.exceptions.IllegalReflectionOperationException;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import static org.reflections.ReflectionUtils.getAllFields;

/**
 * Converts Node/Relations to Objects
 */
public class Beanify {

  private static final Logger LOG = LoggerFactory.getLogger(Beanify.class);

  //Ensure that no one instantiates this util class
  private Beanify() {
  }

  /**
   * Converts given Node into an object of Class klass
   *
   * @param node  Neo4j Node
   * @param klass Class
   * @return populated instance of type klass
   * @throws ligo.exceptions.IllegalReflectionOperationException
   */
  public static <T> T get(Node node, Class<T> klass) {

    if (node == null)
      return null;
    T instance = null;
    try {
      instance = klass.getConstructor().newInstance();
    } catch (InvocationTargetException | IllegalAccessException | NoSuchMethodException |
        InstantiationException e) {
      throw new IllegalReflectionOperationException(e);
    }

    Map<String, Object> properties = new HashMap<>();
    for (String key : node.getPropertyKeys()) {
      properties.put(key, node.getProperty(key));
    }
    properties.put("id", node.getId());

    populate(instance, properties);
    return instance;
  }

  /**
   * populates the instance with the given properties. All the property names are expected to be
   * lowercase. So all the camelCase properties in the instance will be changed to lowercase for
   * population.
   *
   * @param instance   instance to be populated
   * @param properties properties
   */
  public static <T> void populate(T instance, Map<String, Object> properties) {

    for (Field field : getAllFields(instance.getClass())) {

      final String propertyName = field.getName().toLowerCase();
      final Object value = properties.get(propertyName);

      try {
        field.setAccessible(true);
        if (value == null && field.getType().isPrimitive()) {
          LOG.trace("Skipping field setting for primitive field : {}", field);
        }
        else {
          field.set(instance, value);
        }
      } catch (IllegalAccessException e) {
        throw new IllegalReflectionOperationException("Problem populating object", e);
      }
    }
  }

  /**
   * Finds out whether the given node has a label that matches the label of the provided klass
   *
   * @param node  Neo4j Node
   * @param klass Class
   * @return true or false
   */
  public static boolean isInstanceOf(Node node, Class<?> klass) {
    final String nodeLabel = EntityUtils.extractNodeLabel(klass);
    final Iterable<Label> labels = node.getLabels();
    for (Label label : labels) {
      if (label.name().equals(nodeLabel)) {
        return true;
      }
    }
    return false;
  }
}
