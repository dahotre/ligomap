package ligo.utils;

/**
 * 
 * @author hsaqallah
 */
public class DBPropException extends RuntimeException {
  public DBPropException(String propertyName) {
    super(String.format("Property %s was not set.", propertyName));
  }
}
