package ligo.config;

import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import ligo.meta.Entity;
import ligo.meta.IndexType;
import ligo.meta.Indexed;
import ligo.utils.DBProp;
import ligo.utils.EntityUtils;
import org.neo4j.graphdb.*;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.factory.GraphDatabaseSettings;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.IndexManager;
import org.neo4j.graphdb.schema.IndexDefinition;
import org.neo4j.helpers.collection.MapUtil;
import org.neo4j.tooling.GlobalGraphOperations;
import org.reflections.ReflectionUtils;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Configures Neo start/shut
 */
public class DBConfig {

  private static final Logger LOG = LoggerFactory.getLogger(DBConfig.class);

  public static final String MODEL_PACKAGES = "MODEL_PACKAGES";
  public static final String DB_PATH = "DB_PATH";
  public static final String SHOULD_WARM_INDEXES = "SHOULD_WARM_INDEXES";
  public static final String INDEX_HIT_THRESHOLD = "INDEX_HIT_THRESHOLD";

  public static boolean isDbOn = false;
  private static String dbPath;
  private static ImmutableMap<String, Index<Node>> FULLTEXT_BY_NAME_MAP;
  private static ImmutableMultimap<Class<?>, Index<Node>> FULLTEXT_BY_CLASS_MAP;
  private GraphDatabaseService db;
  private static String[] modelPackages;
  private static Reflections modelReflections;
  private boolean shouldWarmIndexes = false;

  public DBConfig() {
    dbPath = DBProp.getDBProperty(DB_PATH);
    modelPackages = DBProp.<String>getDBProperty(MODEL_PACKAGES).split(",");
    String warmIndexString = DBProp.getDBProperty(SHOULD_WARM_INDEXES);
    shouldWarmIndexes = ( !Strings.isNullOrEmpty(warmIndexString)
        && warmIndexString.toLowerCase().equals("true"));

    for (String modelPackage : getModelPackages()) {
      if (modelReflections == null) {
        modelReflections = new Reflections(modelPackage);
      }
      else {
        modelReflections.merge(new Reflections(modelPackage));
      }
    }
    start();
  }

  public String[] getModelPackages() {
    return modelPackages;
  }

  public Reflections getModelReflections() {
    return modelReflections;
  }

  /**
   * Get the full text search index by it's name
   *
   * @param indexName Exact name of the index.This will be found on the @Indexed annotation
   * @return Index
   */
  public static Index<Node> getFullTextIndex(String indexName) {
    return FULLTEXT_BY_NAME_MAP.get(indexName);
  }

  /**
   * Get the full text search indexes for the given class
   *
   * @param klass Class
   * @return Collection of Indexes
   */
  public static Collection<Index<Node>> getFullTextIndexes(Class<?> klass) {
    return FULLTEXT_BY_CLASS_MAP.get(klass);
  }

  public void start() {
    LOG.info("Loading DB from {}", dbPath);
    db =
        new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(dbPath)
            .setConfig(GraphDatabaseSettings.allow_store_upgrade, "true")
            .setConfig(GraphDatabaseSettings.node_auto_indexing, "true").newGraphDatabase();
    initIndexes(db);
    isDbOn = true;
    registerShutdownHook(db);
  }

  private void registerShutdownHook(final GraphDatabaseService db) {
    Runtime.getRuntime().addShutdownHook(new Thread() {
      @Override
      public void run() {
        LOG.info("DB going down. Bye!");
        db.shutdown();
      }
    });
  }

  public GraphDatabaseService getDb() {
    if (!isDbOn) {
      start();
    }
    return db;
  }

  private void initIndexes(final GraphDatabaseService db) {
    Set<Class<?>> entities = new HashSet<>();
    for (String modelPackage : modelPackages) {
      Reflections reflections = new Reflections(modelPackage);
      entities.addAll(reflections.getTypesAnnotatedWith(Entity.class));
    }
    LOG.info("DB has {} entities total.", entities.size());

    final ImmutableMap.Builder<String, Index<Node>> fullTextIndexBuilder = ImmutableMap.builder();
    final ImmutableMultimap.Builder<Class<?>, Index<Node>> fullTextByClassBuilder =
        ImmutableMultimap.
            builder();
    try (Transaction tx = db.beginTx()) {

      for (Class<?> entity : entities) {
        Set<Field> indexedFields = ReflectionUtils.
            getAllFields(entity, ReflectionUtils.withAnnotation(Indexed.class));

        final Label label = DynamicLabel.label(EntityUtils.extractNodeLabel(entity));

        //Get EXACT indexes for a given label
        final Iterable<IndexDefinition> schemaIndexes = db.schema().getIndexes(label);
        for (Field indexedField : indexedFields) {
          final Indexed indexedAnnotation = indexedField.getAnnotation(Indexed.class);

          INDEX_SWITCH:
          switch (indexedAnnotation.type()) {
            case EXACT:

              String indexableProperty = indexedField.getName().toLowerCase();

              for (IndexDefinition schemaIndex : schemaIndexes) {
                if (schemaIndex.getPropertyKeys().iterator().hasNext()
                    && schemaIndex.getPropertyKeys().iterator().next().equals(indexableProperty)) {

                  break INDEX_SWITCH;
                }
              }

              db.schema().indexFor(label).on(indexableProperty).create();
              LOG.info("Created index for {} on {}", label, indexableProperty);
              break;

            case FULL_TEXT:

              final Index<Node> nodeIndex = db.index().forNodes(indexedAnnotation.name(),
                  MapUtil.stringMap(IndexManager.PROVIDER, "lucene", "type", "fulltext"));

              fullTextIndexBuilder.put(indexedAnnotation.name(), nodeIndex);
              fullTextByClassBuilder.put(entity, nodeIndex);
              LOG.info("Init index {}", indexedAnnotation.name());

              break;
          }
        }
      }
      FULLTEXT_BY_NAME_MAP = fullTextIndexBuilder.build();
      FULLTEXT_BY_CLASS_MAP = fullTextByClassBuilder.build();

      tx.success();
    }
    LOG.info("Dumping FULLTEXT_BY_CLASS_MAP : {}", Joiner.on(",\n")
        .withKeyValueSeparator(" : ")
        .join(FULLTEXT_BY_CLASS_MAP.asMap()));
    LOG.info("Dumping FULLTEXT_BY_NAME_MAP : {}", Joiner.on(",\n")
        .withKeyValueSeparator(" : ")
        .join(FULLTEXT_BY_NAME_MAP));
    if (shouldWarmIndexes) {
      warmupIndexes();
    }
  }

  private void warmupIndexes() {
    LOG.info("creating indexes..");
    for (Class<?> klass : FULLTEXT_BY_CLASS_MAP.keySet()) {
      Set<Field> indexable = EntityUtils.extractIndexable(klass);
      try (Transaction tx = db.beginTx()) {
        ResourceIterable<Node> nodes =
            GlobalGraphOperations.at(db).getAllNodesWithLabel(DynamicLabel.label(EntityUtils.extractNodeLabel(klass)));
        int nodesCounter = 0;
        for (Node node : nodes) {
          List<String> nodePropertyKeys = Lists.newArrayList(node.getPropertyKeys());
          for (final Index<Node> nodeIndex : FULLTEXT_BY_CLASS_MAP.get(klass)) {
            Optional<Field> optionalMatchingField = Iterables.tryFind(indexable, new Predicate<Field>() {
              @Override
              public boolean apply(@Nonnull Field input) {
                Indexed indexedAnnotation = input.getAnnotation(Indexed.class);
                if (indexedAnnotation.type().equals(IndexType.FULL_TEXT)) {
                  return indexedAnnotation.name().equals(nodeIndex.getName());
                }
                return false;
              }
            });
            if (optionalMatchingField.isPresent() &&
                nodePropertyKeys.contains(optionalMatchingField.get().getName())) {
              LOG.info("Index put: ({} : {}, {}, {})",
                  nodeIndex.getName(),
                  node.getId(),
                  optionalMatchingField.get().getName(),
                  node.getProperty(optionalMatchingField.get().getName()));

              nodeIndex.add(node, optionalMatchingField.get().getName(),
                  node.getProperty(optionalMatchingField.get().getName()));
            }
          }
          ++nodesCounter;
        }
        LOG.info("{} nodes indexed for Label {}", nodesCounter, EntityUtils.extractNodeLabel(klass));
        tx.success();
      }
    }
  }
  

}
