package ligo.repository;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import ligo.config.DBConfig;
import ligo.exceptions.IllegalDBOperation;
import ligo.exceptions.IllegalLabelExtractionAttemptException;
import ligo.exceptions.IllegalReflectionOperationException;
import ligo.meta.BaseRelationship;
import ligo.meta.Entity;
import ligo.meta.IndexType;
import ligo.meta.Indexed;
import ligo.utils.Beanify;
import ligo.utils.DBProp;
import ligo.utils.EntityUtils;
import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.cypher.javacompat.ExecutionResult;
import org.neo4j.graphdb.*;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.IndexHits;
import org.neo4j.helpers.collection.IteratorUtil;
import org.neo4j.tooling.GlobalGraphOperations;
import org.reflections.ReflectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.lang.reflect.Field;
import java.util.*;

import static org.neo4j.graphdb.DynamicLabel.label;

/**
 * Basic CRUD for a given Entity
 */
public abstract class EntityRepo {

  private static final DBConfig DEFAULT_DB_CONFIG = new DBConfig();

  private static final Logger LOG = LoggerFactory.getLogger(EntityRepo.class);
  public static final String UPDATED_AT_LONG = "uat";
  public static final String CREATED_AT_LONG = "cat";

  protected GraphDatabaseService db;
  protected ExecutionEngine engine;

  public EntityRepo() {
    this.db = DEFAULT_DB_CONFIG.getDb();
    this.engine = new ExecutionEngine(db);
  }

  protected <T> List<T> findAll(final Class<T> klass) {
    final Label label = label(EntityUtils.extractNodeLabel(klass));
    List<T> tList;
    try (Transaction tx = db.beginTx()) {
      final ResourceIterable<Node> allNodesWithLabel =
          GlobalGraphOperations.at(db).getAllNodesWithLabel(label);
      tList = Lists.newArrayList(Iterables.transform(allNodesWithLabel, new Function<Node, T>() {
        @Override
        public T apply(@Nullable Node input) {
          return Beanify.get(input, klass);
        }
      }));
      tx.success();
    }
    return tList;
  }

  /**
   * Find by property key-value for the given klass
   *
   * @param key   key
   * @param value value to be searched. Although this is Object, anything other than
   *              primitives, String, or Array will result in problems.
   * @param klass Class of expected object
   * @return Object found in DB
   * @throws IllegalLabelExtractionAttemptException
   */
  protected <T> List<T> find(final String key, final Object value, Class<T> klass) {
    String labelName = EntityUtils.extractNodeLabel(klass);
    List<T> tList = null;
    try (Transaction tx = db.beginTx();
         ResourceIterator<Node> nodes =
             db.findNodesByLabelAndProperty(label(labelName), key, value).iterator()) {

      while (nodes != null && nodes.hasNext()) {
        if (tList == null) {
          tList = Lists.newArrayList();
        }
        tList.add(Beanify.get(nodes.next(), klass));

      }
      tx.success();
    }
    return tList;
  }


  /**
   * Searches for the given query in the specified index name
   *
   * @param key Name of field to search
   * @param query Query string
   * @param klass Class of the expected result
   * @return List of objects that match the search criteria
   */
  protected <T> List<T> search(final String key, final String query, Class<T> klass) {
    Preconditions.checkNotNull(key);
    Preconditions.checkNotNull(query);
    Preconditions.checkNotNull(klass);

    List<T> tList = find(key, query, klass);
    if (tList != null && !tList.isEmpty()) {
      return tList;
    }

    final Set<Field> matchingFields = ReflectionUtils.getAllFields(klass, ReflectionUtils.withName(key));
    if (matchingFields.iterator().hasNext()) {

      final Field matchingField = matchingFields.iterator().next();
      String indexName = null;
      if (matchingField.isAnnotationPresent(Indexed.class) &&
          matchingField.getAnnotation(Indexed.class).type().equals(IndexType.FULL_TEXT) &&
          (indexName = matchingField.getAnnotation(Indexed.class).name()) != null ) {

        try (Transaction tx = db.beginTx()) {
          final Index<Node> fullTextIndex = DBConfig.getFullTextIndex(indexName);
            try (IndexHits<Node> hits = fullTextIndex.query(key, query)) {
              for (Node hit : hits) {
                if (hits.currentScore() < Float.parseFloat(DBProp.getDBProperty(DBConfig.INDEX_HIT_THRESHOLD))) {
                  break;
                }
                if (tList == null) {
                  tList = Lists.newArrayList();
                }
                tList.add(Beanify.get(hit, klass));
                LOG.debug("Score is {} for result: ({},{}) for query: {}",
                    hits.currentScore(), key, hit.getProperty(key), query);
              }
              hits.close();
            }
          tx.success();
        }
      }

    }
    else {
      throw new IllegalReflectionOperationException("No property with name:" + key);
    }

    return tList;
  }

  /**
   * Find a node of given class, by it's id.
   *
   * @param id    long id
   * @param klass class
   * @return Object found in DB
   */
  protected <T> T find(final Long id, Class<T> klass) {
    T t = null;
    try (Transaction tx = db.beginTx()) {
      try {
        Node nodeById = db.getNodeById(id);
        t = Beanify.get(nodeById, klass);
      } catch (NotFoundException e) {
        LOG.debug("Node {} not found for class {}", id, klass);
      }
      tx.success();
    }
    return t;
  }

  /**
   * Find nodes for given list of ids
   *
   * @param ids Node ids
   * @param klass Class
   * @return Nodes
   */
  protected <T> List<T> find(Class<T> klass, final Long... ids) {
    Preconditions.checkNotNull(ids);
    Preconditions.checkNotNull(klass);
    final String nodeLabel = EntityUtils.extractNodeLabel(klass);

    final String query = String.format("MATCH (x:%s) WHERE ID(x) IN [%s] RETURN x", nodeLabel,
        Joiner.on(",").skipNulls().join(ids));
    return singleProjectionQuery(query, Maps.newHashMap(), "x", klass);
  }
  /**
   * Delete node of given class, on basis of the given property key-value
   *
   * @param klass Class to which the object belongs
   * @param key   key
   * @param value value
   */
  public <T> void delete(final Class<T> klass, final String key, final String value) {
    final Collection<Index<Node>> fullTextIndexes = DBConfig.getFullTextIndexes(klass);

    try (Transaction tx = db.beginTx();
         ResourceIterator<Node> iterator =
             db.findNodesByLabelAndProperty(label(EntityUtils.extractNodeLabel(klass)), key, value).iterator()) {
      while (iterator.hasNext()) {
        final Node node = iterator.next();
        for (Relationship relationship : node.getRelationships()) {
          LOG.info("Deleting relationship {}", relationshipToString(relationship));
          relationship.delete();
        }
        for (Index<Node> fullTextIndex : fullTextIndexes) {
          fullTextIndex.remove(node);
        }
        LOG.info("Deleting node {}", nodeToString(node));
        node.delete();
      }
      iterator.close();
      tx.success();
    }
  }

  /**
   * Delete node
   *
   * @param klass Node's label has to match klass label
   * @param id    Node's id has to match id
   */
  public <T> void delete(final Class<T> klass, final long id) {
    final Collection<Index<Node>> fullTextIndexes = DBConfig.getFullTextIndexes(klass);

    try (Transaction tx = db.beginTx()) {
      final Node nodeById = db.getNodeById(id);
      if (nodeById.hasLabel(label(EntityUtils.extractNodeLabel(klass)))) {
        for (Relationship relationship : nodeById.getRelationships()) {
          LOG.info("Deleting relationship {}", relationshipToString(relationship));
          relationship.delete();
        }
        for (Index<Node> fullTextIndex : fullTextIndexes) {
          fullTextIndex.remove(nodeById);
        }
        LOG.info("Deleting node {}", nodeToString(nodeById));
        nodeById.delete();
      } else {
        LOG.warn("Nothing to delete.");
      }
      tx.success();
    }
  }

  /**
   * Drops all nodes corresponding to the provided klass
   *
   * @param klass Class
   */
  public void deleteAll(final Class<?> klass) {
    throw new UnsupportedOperationException("Not yet implemented");
  }

  /**
   * If an object with the same Id exists, it will be updated. Use with caution.
   * Else the given instance will be persisted and the resultant node will be returned.
   *
   * @param t Entity to be persisted
   * @param overwriteExisting
   * @return Node representation of Object
   */
  private <T> Node createNode(final T t, boolean overwriteExisting) {
    Map<String, Object> properties = EntityUtils.extractPersistableProperties(t);
    String labelName = EntityUtils.extractNodeLabel(t.getClass());

    Node existingNode = findExistingNode(t);

    if (!overwriteExisting && existingNode != null) {
      return existingNode;
    }

    //Getting ready for node creation. First find full text indexes
    Map<String, String> keyToIndexNameMap = Maps.newHashMap();
    final Set<Field> indexable = EntityUtils.extractIndexable(t.getClass());
    if (indexable != null) {
      for (Field field : indexable) {
        final Indexed indexed = field.getAnnotation(Indexed.class);
        if (indexed.type().equals(IndexType.FULL_TEXT)) {
          keyToIndexNameMap.put(field.getName().toLowerCase(), indexed.name());
        }
      }
    }

    //Next TX to save new node
    try (Transaction tx = db.beginTx()) {
      if (existingNode == null) {
        existingNode = db.createNode(label(labelName));
      }

      final long currentTime = new Date().getTime();
      properties.put(UPDATED_AT_LONG, currentTime);
      if (!properties.containsKey(CREATED_AT_LONG) ||
          properties.get(CREATED_AT_LONG).equals(0l) ) {

        properties.put(CREATED_AT_LONG, currentTime);
      }
      for (Map.Entry<String, Object> prop : properties.entrySet()) {
        if (prop.getValue() != null) {
          existingNode.setProperty(prop.getKey(), prop.getValue());
          if (keyToIndexNameMap.containsKey(prop.getKey())) {
            final Index<Node> fullTextIndex =
                DBConfig.getFullTextIndex(keyToIndexNameMap.get(prop.getKey()));
            fullTextIndex.add(existingNode, prop.getKey(), prop.getValue());
            LOG.debug("Adding to FTS {}:\n---\nNode:{}\n(key,value):({},{})",
                fullTextIndex.getName(),
                nodeToString(existingNode),
                prop.getKey(), prop.getValue());
          }
        }
      }
      LOG.info("Node updated {}", nodeToString(existingNode));
      tx.success();
      return existingNode;
    }

  }

  /**
   * Use this method to get a corresponding DB node for a given Object. If the object has an ID,
   * then the node of the same ID will be retrieved. Else, other properties with the @Unique will be
   * searched for.
   *
   * @param t Entity to be looked up
   * @return DB Node
   */
  protected <T> Node findExistingNode(T t) {
    Node existingNode = null;
    final Map<String, Object> properties = EntityUtils.extractPersistableProperties(t);

    // 1st TX to find existing
    try (Transaction tx = db.beginTx()) {
      Long id;
      if ((id = (Long) properties.get("id")) != null) {
        existingNode = db.getNodeById(id);
      }
      tx.success();
    }

    if (existingNode != null) {
      return existingNode;
    }

    // 2nd TX to find other nodes with same property marked as unique
    final Map<String, Object> uniqueProperties = EntityUtils.extractUniqueProperties(t);
    if (uniqueProperties != null && uniqueProperties.containsValue(null)) {
      throw new IllegalDBOperation("Members with @Unique cannot be null." + uniqueProperties);
    }
    if (existingNode == null && uniqueProperties != null && !uniqueProperties.isEmpty()) {
      try (Transaction tx = db.beginTx()) {
        Label label = label(EntityUtils.extractNodeLabel(t.getClass()));
        for (Map.Entry<String, Object> uniqueProperty : uniqueProperties.entrySet()) {
          final ResourceIterable<Node> nodes =
              db.findNodesByLabelAndProperty(label, uniqueProperty.getKey(), uniqueProperty.getValue());
          if (nodes != null && nodes.iterator() != null && nodes.iterator().hasNext()) {
            existingNode = nodes.iterator().next();

            if (nodes.iterator().hasNext()) {
              LOG.error("More than one {} Node exists with unique property ({},{})",
                  existingNode.getLabels(), uniqueProperty.getKey(), uniqueProperty.getValue());
            }
            break;
          }
        }
        tx.success();
      }
    }
    return existingNode;
  }

  /**
   * If an object with the same Id exists, it will be updated. Else the given instance will be
   * persisted.
   *
   * @param t Instance of Class to be created
   * @return Created instance of class T
   */
  protected final <T> T save(final T t) {
    return save(t, true);
  }

  /**
   * If similar object exists, it will be fetched and returned. Else a new one will be created.
   *
   * @param t Instance of Class to be created
   * @param overwriteExisting Should overwrite existing object with same Id or @Unique property?
   * @return Created instance of class T
   */
  protected final <T> T save(final T t, boolean overwriteExisting) {
    try (Transaction tx = db.beginTx()) {
      T persistedT = Beanify.get(createNode(t, overwriteExisting), (Class<T>) t.getClass());
      tx.success();
      return persistedT;
    }
  }

  /**
   * Fetch relatives of the given node that satisfy the given relationship constraints
   *
   * @param entity       Given node entity
   * @param relationship Given relationship
   * @return Set of relatives
   */
  public <T, V> Set<V> getRelatives(final T entity, final BaseRelationship<T, V> relationship) {
    if (entity == null) {
      throw new IllegalReflectionOperationException("Cannot get relatives from null object");
    }

    try (Transaction tx = db.beginTx()) {
      final Node node = findExistingNode(entity);
      if (node == null) {
        throw new IllegalReflectionOperationException("Cannot get relatives from object that is not in DB");
      }

      final Iterable<Relationship> dbRelationships = node.getRelationships(
          relationship.getRelationType(), relationship.getDirection());
      Set<V> relatives = Sets.newHashSet();
      for (Relationship dbRelationship : dbRelationships) {
        final Class otherNodeType =
            relationship.getRelationType().getOtherNodeType(entity.getClass());
        if (otherNodeType.isInterface()) {

          final Node relativeNode = dbRelationship.getOtherNode(node);
          final String relativeLabel = relativeNode.getLabels().iterator().next().name();

          final Set<Class<?>> otherNodeConcreteTypes =
              DEFAULT_DB_CONFIG.getModelReflections().getSubTypesOf(otherNodeType);

          final Set<Class<?>> classesMatchingRelativeNode =
              Sets.filter(otherNodeConcreteTypes, new Predicate<Class<?>>() {
                @Override
                public boolean apply(@Nonnull Class<?> concreteClass) {
                  if (concreteClass.isInterface()) {
                    return false;
                  }
                  return concreteClass.isAnnotationPresent(Entity.class) &&
                          EntityUtils.extractNodeLabel(concreteClass).equalsIgnoreCase(relativeLabel);
                }
              });

          if (classesMatchingRelativeNode == null || classesMatchingRelativeNode.isEmpty()) {
            LOG.warn("No classes found for label {}", relativeLabel);
            continue;
          }

          if (classesMatchingRelativeNode.size() > 1) {
            LOG.error("Multiple classes with the name {} : {}", relativeLabel, classesMatchingRelativeNode);
          }

          relatives.add(Beanify.get(relativeNode, (Class<V>) classesMatchingRelativeNode.iterator().next()));

        }
        else {
          relatives.add(Beanify.get(dbRelationship.getOtherNode(node), (Class<V>) otherNodeType));
        }
      }

      tx.success();

      return relatives;
    }

  }

  /**
   * Adds relatives to the given entity node. The relative nodes are created if they don't exist.
   * @param entity       Given node entity
   * @param relationship Relationship between entity and relatives
   * @param relatives    Relatives to be added
   */
  public <T, V> void addRelatives(final T entity, BaseRelationship<T, V> relationship,
                                  final V... relatives) {
    if (entity == null) {
      throw new IllegalReflectionOperationException("Cannot get relatives from null object");
    }

    if (relatives == null || relatives[0] == null) {
      throw new IllegalDBOperation("Cannot add null relatives");
    }

    final Long id = EntityUtils.extractId(entity);
    if (id == null) {
      throw new IllegalDBOperation(
          String.format("Starting entity should have a valid id : %s", entity));
    }

    try (Transaction tx = db.beginTx()) {
      final Node node = db.getNodeById(id);

      for (V relative : relatives) {
        Node relativeNode = null;
        try {
          final Long relativesId = EntityUtils.extractId(relative);

          if (relativesId == null) {
            relativeNode = createNode(relative, true);
          } else {
            relativeNode = db.getNodeById(relativesId);
          }
        } catch (IllegalReflectionOperationException e) {
          LOG.error("Skipping relative {} due to problem in extracting its Long id", e);
        } catch (NotFoundException nfe) {
          LOG.debug("Node not found", nfe);
        }

        if (relativeNode == null) {
          throw new IllegalDBOperation(
              "The relative node is null. Here is the relative object : " + relative);
        }
        Relationship persistedRelationship =
            node.createRelationshipTo(relativeNode, relationship.getRelationType());
        if (relationship.getProperties() != null) {
          for (Map.Entry<String, ? extends Object> entry : relationship.getProperties().entrySet()) {
            persistedRelationship.setProperty(entry.getKey(), entry.getValue());
          }
        }
      }

      tx.success();
    }

  }

  /**
   * Execute any single projection Cypher query from here.
   *
   * @param query Parameterized Cypher query with a single projection
   * @param params params map
   * @param projectionKey The alias used in the param query for the node type to be returned.
   * @param klass Class of the return nodes
   * @param <T> Type of the return node
   * @return List of Entities
   */
  protected <T> List<T> singleProjectionQuery(final String query, final Map params,
                                              final String projectionKey, final Class<T> klass) {
    List<T> ts = Lists.newArrayList();

    try(Transaction tx = db.beginTx()) {
      ExecutionResult result = engine.execute(query, params);
      final ResourceIterator<Node> bookNodes = result.columnAs(projectionKey);
      for (Node node : IteratorUtil.asIterable(bookNodes)) {
        T t = Beanify.get(node, klass);
        if (t != null) {
          ts.add(t);
        }
      }
      tx.success();
    }

    return ts;
  }

  /**
   * Top n sorted entities.
   *
   * @param limit int limit
   * @param fieldName name of the field in the DB.
   *
   * @param klass Entity to be sorted
   * @return list
   */
  protected <T> List<T> limitSorted(
      final int limit
      , final String fieldName
      , final Order order
      , final Class<T> klass
  ) {
    Map<String, Object> params = Maps.newHashMapWithExpectedSize(1);
    params.put("limit", limit);

    final String nodeLabel = EntityUtils.extractNodeLabel(klass);
    String parameterizedQuery = String.format("MATCH (x:%s) RETURN x ORDER BY x.%s %s LIMIT {limit}"
        , nodeLabel
        , fieldName.toLowerCase().trim()
        , order.name()
    );
    return singleProjectionQuery(parameterizedQuery, params, "x", klass);
  }

  /**
   * Returns the current DB service in use.
   *
   * @return DB service
   */
  public GraphDatabaseService getDb() {
    return db;
  }

  private String nodeToString(Node node) {
    StringBuilder sb = new StringBuilder(String.format("%s\n---\n", node.getLabels()));
    sb.append(String.format("%s : %s\n", "id", node.getId()));
    for (String key : node.getPropertyKeys()) {
      sb.append(String.format("%s : %s\n", key, node.getProperty(key)));
    }
    return sb.toString();
  }

  private String relationshipToString(Relationship relationship) {
    StringBuilder sb = new StringBuilder(String.format("%s\n---\n", relationship.getType().name()));
    for (String key : relationship.getPropertyKeys()) {
      sb.append(String.format("%s : %s\n", key, relationship.getProperty(key)));
    }
    return sb.toString();
  }

  public enum Order {
    ASC, DESC;
  }

}
