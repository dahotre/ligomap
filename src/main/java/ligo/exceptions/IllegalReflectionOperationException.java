package ligo.exceptions;

/**
 * Combines reflection related exceptions into 1
 */
public class IllegalReflectionOperationException extends RuntimeException {
  public IllegalReflectionOperationException(Throwable cause) {
    super(cause);
  }

  public IllegalReflectionOperationException(String message, Throwable cause) {
    super(message, cause);
  }

  public IllegalReflectionOperationException(String message) {
    super(message);
  }
}
