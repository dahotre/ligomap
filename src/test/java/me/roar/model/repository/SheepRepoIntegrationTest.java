package me.roar.model.repository;

import java.util.Set;
import me.roar.model.node.Animal;
import me.roar.model.node.Lion;
import me.roar.model.node.Sheep;
import org.junit.After;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Tests SheepRepo
 */
public class SheepRepoIntegrationTest {

  private final SheepRepo SHEEP_REPO = new SheepRepo();
  private final LionRepo LION_REPO = new LionRepo();
  private Sheep leaderSheep, followerSheep;
  private Lion lion;

  private final String LEADER_NAME = "Leader sheep 1";
  private final String FOLLOWER_NAME = "Follower sheep 1";
  private final String LION_NAME = "Lion 1";

  @Before
  public void setup() {

    LION_REPO.deleteByName(LION_NAME);
    SHEEP_REPO.deleteByName(FOLLOWER_NAME);
    SHEEP_REPO.deleteByName(LEADER_NAME);

    lion = new Lion().withName(LION_NAME);
    leaderSheep = new Sheep();
    leaderSheep.setName(LEADER_NAME);

    followerSheep = new Sheep();
    followerSheep.setName(FOLLOWER_NAME);
  }

  @Test
  @Ignore
  public void testCreate() {
    final Sheep createdSheep = SHEEP_REPO.create(followerSheep);
    assertNotNull(createdSheep);
    assertNotNull(createdSheep.getId());
    assertNotNull(createdSheep.getUpdatedAt());
    assertNotEquals(Long.valueOf(0), createdSheep.getId());
  }

  @Test
  @Ignore
  public void testFollowsAndGetLeaders() {
    final Sheep createdFollower = SHEEP_REPO.create(followerSheep);
    SHEEP_REPO.follows(createdFollower, leaderSheep);
    SHEEP_REPO.follows(createdFollower, lion);

    final Set<Animal> leaders = SHEEP_REPO.getLeaders(createdFollower);
    assertNotNull(leaders);
    assertEquals(2, leaders.size());
  }

  @After
  public void clean() {
    LION_REPO.deleteByName(LION_NAME);
    SHEEP_REPO.deleteByName(FOLLOWER_NAME);
    SHEEP_REPO.deleteByName(LEADER_NAME);
  }
}
